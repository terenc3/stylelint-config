// SPDX-FileCopyrightText: 2023 Benjamin Kahlau <terenc3@roanapur.de>
//
// SPDX-License-Identifier: GPL-3.0-only

import configRecommended from 'stylelint-config-recommended'
import configStandard from 'stylelint-config-standard'

export default {
    rules: {
        ...configRecommended.rules,
        ...configStandard.rules,
        'at-rule-no-unknown': [
            true,
            {
                ignoreAtRules: ['tailwind']
            }
        ]
    }
}
