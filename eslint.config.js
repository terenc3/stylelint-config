// SPDX-FileCopyrightText: 2023 Benjamin Kahlau <terenc3@roanapur.de>
//
// SPDX-License-Identifier: GPL-3.0-only

export { default } from '@terenc3/eslint-config'
