<!--
SPDX-FileCopyrightText: 2023 Benjamin Kahlau <terenc3@roanapur.de>

SPDX-License-Identifier: GPL-3.0-only
-->

# stylelint config

Sharable stylelint config

## Content

-   Extends stylelint-config-standard
-   Allows @tailwind

## Installation

```shell
npm config set @terenc3:registry https://codeberg.org/api/packages/terenc3/npm/
npm i -D @terenc3/stylelint-config
```

### stylelint.config.mjs

For some reason the file needs to have `*.mjs` suffix even if everthing is ESM!

```js
export { default } from '@terenc3/stylelint-config'
```
